import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String ulang;
        Scanner pilih = new Scanner(System.in);
        int pilihan = Menu.menu_utama();
        do {
            if (pilihan == 1) {
                    Scanner bidang = new Scanner(System.in);
                    Menu.sub_menu_luas();
                    int pilBidang = bidang.nextInt();

                    if (pilBidang == 1){
                        BidangPersegi.hitungBidangPersegi();
                    } else if (pilBidang == 2) {
                        BidangPersegiPanjang.hitungBidangPersegiPangjang();
                    } else if (pilBidang == 3) {
                        Lingkaran.hitungLingkaran();
                    } else if (pilBidang == 4) {
                        Segitiga.hitungSegituga();
                    } else if (pilBidang == 0) {
                        Menu.menu_utama();
                    }


            } else if (pilihan == 2) {
                    Scanner bidang = new Scanner(System.in);
                    Menu.sub_menu_volume();
                    int pilBidang = bidang.nextInt();

                    if (pilBidang == 1){
                        Kubus.hitungKubus();
                    } else if (pilBidang == 2) {
                        Balok.hitungBalok();
                    } else if (pilBidang == 3) {
                        Tabung.hitungTabung();
                    } else if (pilihan == 0){
                        Menu.menu_utama();
                    }


            } else if (pilihan == 0){
                System.exit(0);
            }

            System.out.print("\nIngin mencoba lagi? (y/t) = ");
            ulang = pilih.next();
            System.out.print("\n");
        } while (ulang.equals("Y") || ulang.equals("y"));
    }
}
