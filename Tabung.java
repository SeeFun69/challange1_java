import java.util.Scanner;

public class Tabung {
    static void hitungTabung(){


        Scanner input = new Scanner(System.in);

        System.out.println("Volume Tabung : Luas Alas x Tinggi");

        System.out.print("Jari-jari alas: ");
        double r = input.nextDouble();
        System.out.print("Tinggi : ");
        double t = input.nextDouble();

        double luasLingkaran = (22 * r * r)/7;
        double volTabung = luasLingkaran * t;

        System.out.println("Volume Tabung = " + volTabung);
    }
}
