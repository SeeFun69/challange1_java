import java.util.Scanner;

public class Kubus {
    static void hitungKubus(){
        System.out.println("Volume Kubus           : s x s x s");

        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan nilai sisi : ");
        int sisi = input.nextInt();

        System.out.println("Volume Kubus : " + (sisi * sisi * sisi));
    }
}
