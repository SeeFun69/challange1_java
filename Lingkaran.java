import java.util.Scanner;

public class Lingkaran {
    static void hitungLingkaran(){
        Scanner input = new Scanner(System.in);

        System.out.println("Luas Lingkaran : phi x r x r");

        System.out.print("Jari-jari alas: ");
        double r = input.nextDouble();

        double luasLingkaran = (22 * r * r)/7;

        System.out.println(" Luas Lingkaran adalah " + luasLingkaran);
    }
}
