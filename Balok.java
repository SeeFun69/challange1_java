import java.util.Scanner;

public class Balok {
    static void hitungBalok(){
        Scanner input = new Scanner(System.in);

        System.out.println("Volume Balok  : p x l x t");

        System.out.print("Masukkan nilai panjang : ");
        int panjang = input.nextInt();

        System.out.print("Masukkan nilai lebar : ");
        int lebar = input.nextInt();

        System.out.print("Masukkan nilai tinggi : ");
        int tinggi = input.nextInt();

        System.out.println("Volume Balok : " + panjang * lebar * tinggi);
    }
}
